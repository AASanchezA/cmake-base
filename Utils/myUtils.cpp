// Some stuff
#include "Utils.hpp"

#include "Table.h"

#include <cstdlib>
#include <iostream>
#include <cmath>

double mySqrt( double number )
{
    std::cout << "My Custom implementation of square root\n";
    return std::sqrt( number );
}
