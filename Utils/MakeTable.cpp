// A simple progrom that build a sqrt table
#include <cstdlib>
#include <cstdio>
#include <cmath>

int main (int argc, char *argv[])
{
    if (argc < 2)
        return 1;

    double result;

    // Open a file
    FILE *fout = std::fopen(argv[1],"w");
    if(!fout)
    {
        std::perror("not able to open file");
        return EXIT_FAILURE;
    }
    
    fprintf( fout, "double square_root[] = {\n");
    for( int i = 0; i < 10; ++i)
    {
        result = sqrt( static_cast<double>(i));
        fprintf( fout, "%g,\n", result);
    }
    fprintf( fout, "0};\n");
    std::fclose(fout);

    return 0;
}