// A simple program that computes the square root of a number
#include <cstdlib>
#include <iostream>
#include <cmath>

#include "AppConfig.h"

#ifdef USE_UTILS
#include "Utils.hpp"
#endif

int main (int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cout << "Usage: " << argv[0] << " number\n";
    std::cout << "Major Version : " << App_VERSION_MAJOR << "\n";
    std::cout << "Minor Version : " << App_VERSION_MINOR << "\n";
    return 1;
  }

  std::cout << "Test Passphrase..\n";


  double inputValue = atof(argv[1]);

#if defined (HAVE_LOG) && defined (HAVE_EXP)
    std::cout << "Using Log and exp\n";
    double outputValue = exp(log(inputValue)*0.5);
#else
  #ifdef USE_UTILS
    double outputValue = mySqrt( inputValue );
  #else
    double outputValue = std::sqrt(inputValue);
  #endif
#endif
  std::cout << "The square root of " << inputValue << " is " << outputValue << "\n";
  return 0;
}
